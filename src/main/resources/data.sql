create table if not exists setor (
	ID BIGINT NOT NULL PRIMARY KEY,
	DESCRICAO VARCHAR NOT NULL);
	
insert into setor
values(96099, 'RH');

insert into setor
values(435099, 'Desenvolvimento');

create table if not exists colaborador (
	CPF BIGINT NOT NULL PRIMARY KEY,
	NOME VARCHAR NOT NULL,
	TELEFONE BIGINT NOT NULL,
	EMAIL VARCHAR NOT NULL,
	SETOR_ID BIGINT NOT NULL,
	FOREIGN KEY (SETOR_ID) 
    REFERENCES public.setor(ID));
  
insert into colaborador
values(6968573083, 'Eduardo Sphurs', 'eduardo@bol.com.br', 21981324556, 96099);

insert into colaborador
values(26960996007, 'Marcelo Trigueiro', 'marcelo@bol.com.br', 3194225487, 96099);


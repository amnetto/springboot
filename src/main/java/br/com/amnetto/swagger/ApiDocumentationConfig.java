package br.com.amnetto.swagger;

import io.swagger.annotations.SwaggerDefinition;

@SwaggerDefinition(
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"},
        schemes = {SwaggerDefinition.Scheme.HTTP, SwaggerDefinition.Scheme.HTTPS}
)
public interface ApiDocumentationConfig {

}
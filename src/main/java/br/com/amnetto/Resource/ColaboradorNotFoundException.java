package br.com.amnetto.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ColaboradorNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -4064401457424191079L;

	public ColaboradorNotFoundException(String exception) {
		super(exception);
	}

}

package br.com.amnetto.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SetorNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -6925824546686071553L;

	public SetorNotFoundException(String exception) {
		super(exception);
	}
}

package br.com.amnetto.Resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.amnetto.model.Colaborador;
import br.com.amnetto.model.ColaboradorRepository;
import io.swagger.annotations.ApiOperation;

@RestController
public class ColaboradorResource {

	@Autowired
	private ColaboradorRepository colaboradorRepository;

	@GetMapping("/colaborador")
	public List<Colaborador> retrieveAllColaborador() {
		return colaboradorRepository.findAll();
	}

	@GetMapping("/colaborador/{cpf}")
	@ApiOperation(value = "encontrar o colaborador pelo CPF")
	public Resource<Colaborador> retrieveColaborador(@PathVariable long cpf) {
		Optional<Colaborador> colaborador = colaboradorRepository.findById(cpf);

		if (!colaborador.isPresent())
			throw new ColaboradorNotFoundException("CPF:" + cpf);

		Resource<Colaborador> resource = new Resource<Colaborador>(colaborador.get());

		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllColaborador());

		resource.add(linkTo.withRel("todos os colaboradores"));

		return resource;
	}
	
	@GetMapping("/colaborador/setor/{idSetor}")
	@ApiOperation(value = "encontrar o colaboradores pelo ID do setor")
	public List<Colaborador> retrieveColaboradoresPorSetor(@PathVariable long idSetor) {
		return colaboradorRepository.findByIdSetor(idSetor);
	}

	@DeleteMapping("/colaborador/{cpf}")
	public void deleteColaborador(@PathVariable long cpf) {
		colaboradorRepository.deleteById(cpf);
	}

	@PostMapping("/colaborador")
	public ResponseEntity<Object> createColaborador(@RequestBody Colaborador colaborador) {
		Colaborador savedColaborador = colaboradorRepository.save(colaborador);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{cpf}")
				.buildAndExpand(savedColaborador.getCpf()).toUri();

		return ResponseEntity.created(location).build();

	}
	
	@PutMapping("/colaborador/{cpf}")
	public ResponseEntity<Object> updateColaborador(@RequestBody Colaborador colaborador, @PathVariable long cpf) {
		Optional<Colaborador> colaboradorOptional = colaboradorRepository.findById(cpf);

		if (!colaboradorOptional.isPresent())
			return ResponseEntity.notFound().build();

		colaborador.setCpf(cpf);
		
		colaboradorRepository.save(colaborador);

		return ResponseEntity.noContent().build();
	}
}

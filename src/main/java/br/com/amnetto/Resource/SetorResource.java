package br.com.amnetto.Resource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.amnetto.model.Setor;
import br.com.amnetto.model.SetorRepository;
import io.swagger.annotations.ApiOperation;

@RestController
public class SetorResource {

	@Autowired
	private SetorRepository setorRepository;

	@GetMapping("/setor")
	public List<Setor> retrieveAllSetor() {
		return setorRepository.findAll();
	}

	@GetMapping("/setor/{id}")
	@ApiOperation(value = "encontrar o colaborador pelo ID")
	public Resource<Setor> retrieveSetor(@PathVariable long id) {
		Optional<Setor> setor = setorRepository.findById(id);

		if (!setor.isPresent())
			throw new SetorNotFoundException("id:" + id);

		Resource<Setor> resource = new Resource<Setor>(setor.get());

		ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).retrieveAllSetor());

		resource.add(linkTo.withRel("todos os setores"));

		return resource;
	}

	@DeleteMapping("/setor/{id}")
	public void deleteSetor(@PathVariable long id) {
		setorRepository.deleteById(id);
	}

	@PostMapping("/setor")
	public ResponseEntity<Object> createSetor(@RequestBody Setor setor) {
		Setor savedSetor = setorRepository.save(setor);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(savedSetor.getId()).toUri();

		return ResponseEntity.created(location).build();

	}
	
	@PutMapping("/setor/{id}")
	public ResponseEntity<Object> updateSetor(@RequestBody Setor setor, @PathVariable long id) {
		Optional<Setor> setorOptional = setorRepository.findById(id);

		if (!setorOptional.isPresent())
			return ResponseEntity.notFound().build();

		setor.setId(id);
		
		setorRepository.save(setor);

		return ResponseEntity.noContent().build();
	}
}

package br.com.amnetto.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel
@XmlRootElement
public class Setor {

	@Id
	@GeneratedValue
	private long id;
	
	@ApiModelProperty(notes="decrição deve ter ao mennos 2 caracteres")
	@Size(min=2, message="decrição deve ter ao mennos 2 caracteres")
	private String descricao;
	
	public Setor() {
		super();
	}
	
	public Setor(long id) {
		this();
		this.id = id;
	}

	public Setor(long id, String descricao) {
		this(id);
		this.descricao = descricao;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}

package br.com.amnetto.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SetorRepository  extends JpaRepository<Setor, Long>{

}

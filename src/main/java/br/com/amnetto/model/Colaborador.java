package br.com.amnetto.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@ApiModel
@XmlRootElement
public class Colaborador {
	
	@Id
	@GeneratedValue
	private long cpf;
	
	@ApiModelProperty(notes="Nome deve ter ao mennos 2 caracteres")
	@Size(min=2, message="Nome deve ter ao mennos 2 caracteres")
	private String nome;
	
	@ApiModelProperty
	private long telefone;
	
	@ApiModelProperty
	private String email;
	
	@ApiModelProperty
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "setor_id")
    private Setor setor;

	public Colaborador() {
		super();
	}

	public Colaborador(long cpf, String nome, long telefone, String email, long setorId) {
		this();
		this.cpf = cpf;
		this.nome = nome;
		this.telefone = telefone;
		this.email = email;
		this.setor = new Setor(setorId);
	}
	
	public long getCpf() {
		return cpf;
	}
	public void setCpf(long cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public long getTelefone() {
		return telefone;
	}
	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Setor getSetor() {
		return setor;
	}
	public void setSetor(Setor setor) {
		this.setor = setor;
	}
		
}

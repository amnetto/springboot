package br.com.amnetto.model;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ColaboradorRepository extends JpaRepository<Colaborador, Long>{

	@Query("SELECT c FROM Colaborador c WHERE c.setor.id = ?1")
    List<Colaborador> findByIdSetor(Long id);
}

package br.com.amnetto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.amnetto.Resource.ColaboradorNotFoundException;
import br.com.amnetto.Resource.ColaboradorResource;
import br.com.amnetto.Resource.SetorNotFoundException;
import br.com.amnetto.Resource.SetorResource;
import br.com.amnetto.model.Colaborador;
import br.com.amnetto.model.Setor;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest
public class CrudRestApplicationTests {

	@Autowired
	private ColaboradorResource colaboradorResource;
	
	@Autowired
	private SetorResource setorResource;
    
	private long setorIdMock = 1L;
	private long colaboradorIdMock = 2L;
	
	@Test
	public void Test01_CreateSetor() {
		Setor setor = new Setor(setorIdMock, "TI");
		ResponseEntity<Object> response = setorResource.createSetor(setor);		
		assertEquals(201, response.getStatusCodeValue());
	}
	
	@Test
	public void Test02_CreateColaborador() {
		Colaborador colaborador = new Colaborador(colaboradorIdMock, "Rafael Moreira", 1195325696L, "rafael@bol.com.br", setorIdMock);
		ResponseEntity<Object> response = colaboradorResource.createColaborador(colaborador);		
		assertEquals(201, response.getStatusCodeValue());
	}
	
	@Test
	public void Test03_RetrieveAllSetor() {
		List<Setor> setores = setorResource.retrieveAllSetor();
		assertTrue(setores.size() == 3);
		assertTrue(setores.stream().filter(s -> (s.getId() == setorIdMock)).findFirst().isPresent());
	}
	
	@Test
	public void Test04_RetrieveAllColaborador() {
		List<Colaborador> colaboradores = colaboradorResource.retrieveAllColaborador();
		assertTrue(colaboradores.size() == 3);
		assertTrue(colaboradores.stream().filter(c -> (c.getCpf() == colaboradorIdMock)).findFirst().isPresent());
	}
	
	@Test
	public void Test05_RetrieveBySetorId() {
		Resource<Setor> setor = setorResource.retrieveSetor(setorIdMock);
		assertNotNull(setor.getContent());
		assertEquals(setorIdMock, setor.getContent().getId());
	}
	
	@Test
	public void Test06_RetrieveByColaboradorId() {
		Resource<Colaborador> colaborador = colaboradorResource.retrieveColaborador(colaboradorIdMock);
		assertNotNull(colaborador.getContent());
		assertEquals(colaboradorIdMock, colaborador.getContent().getCpf());
	}
	
	@Test
	public void Test07_RetrieveByColaboradorSetor() {
		List<Colaborador> colaboradores = colaboradorResource.retrieveColaboradoresPorSetor(setorIdMock);
		assertNotNull(colaboradores);
		assertEquals(1, colaboradores.size());
		assertEquals(colaboradorIdMock, colaboradores.get(0).getCpf());
	}
	
	@Test
	public void Test08_DeleteColaborador() {
		colaboradorResource.deleteColaborador(colaboradorIdMock);
		List<Colaborador> colaboradores = colaboradorResource.retrieveAllColaborador();
		assertEquals(2, colaboradores.size());
	}
	
	@Test
	public void Test09_DeleteSetor() {
		setorResource.deleteSetor(setorIdMock);
		List<Setor> setores = setorResource.retrieveAllSetor();
		assertEquals(2, setores.size());
	}
	
	@Test(expected = SetorNotFoundException.class)
	public void Test10_RetrieveBySetorId_Exception() {
		setorResource.retrieveSetor(setorIdMock);
	}
	
	@Test(expected = ColaboradorNotFoundException.class)
	public void Test11_RetrieveByColaboradorId_Exception() {
		colaboradorResource.retrieveColaborador(colaboradorIdMock);
	}
	
}
